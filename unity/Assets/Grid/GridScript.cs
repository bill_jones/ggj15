﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridScript : MonoBehaviour {

	public GameObject TilePrefab;
	public GameObject SpriteManager;

	public int GridWidth;
	public int GridHeight;
	public float TileSpacing;

	public GameObject[,] _tiles;

	// Use this for initialization
	void Start () 
	{
		var tiles = GetComponentsInChildren<TileScript>();
		_tiles = new GameObject[GridWidth,GridHeight];
		foreach(var tile in tiles)
		{
			_tiles[tile.X,tile.Y] = tile.gameObject;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public GameObject GetTile(int x, int y)
	{
		return _tiles[x,y];
	}

	public void CreateGrid()
	{
		Debug.Log("Creating Grid");
		DestroyAllChildren();
		for(var x = 0; x < GridWidth; x++)
		{
			for(var y = 0; y < GridHeight; y ++)
			{
				CreateTile(x, y);
			}
		}
	}

	private void CreateTile(int x , int y)
	{
		var tile = (GameObject)Instantiate(TilePrefab, Vector3.zero, Quaternion.identity);
		tile.name = "Tile (" + x + "," + y + ")";

		var tileScript = tile.GetComponent<TileScript>();
		tileScript.X = x;
		tileScript.Y = y;

		var parentPosition = this.gameObject.transform.position;
		tile.gameObject.transform.SetParent(this.gameObject.transform, false);

		var xPosition = x * TileSpacing;
		var yPosition = y * TileSpacing;
		var vector = new Vector3(xPosition, yPosition, 0);
		tile.gameObject.transform.localPosition = vector;

		this.gameObject.transform.position = parentPosition;

		var render = tile.GetComponent<SpriteRenderer>();
		SetSpriteForTile(x, y, render);
	}

	public void DestroyAllChildren()
	{
		var children = new Transform[this.transform.childCount];
		for(var i = 0; i < this.transform.childCount; i++)
		{
			children[i] = this.transform.GetChild(i);
		}

		foreach(var transform in children)
		{
			DestroyImmediate(transform.gameObject);
		}
	}

	private void SetFenceCollider(GameObject gameObject)
	{
		gameObject.AddComponent<BoxCollider2D>();
		gameObject.layer = 8; // Fence
	}

	public void SetSpriteForTile(int x, int y, SpriteRenderer render)
	{
		var sprites = SpriteManager.GetComponent<SpritesScript>();
		var gridPaddingSides = 3;
		var gridPaddingTop = 3;
	
		if(x == gridPaddingSides -1 && 
		   y > gridPaddingTop - 1 &&
		   y < GridHeight - gridPaddingTop)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.FenceLeft;
			SetFenceCollider(render.gameObject);
			return;
		}
	
		if(x == (GridWidth - gridPaddingSides) &&
		   y > gridPaddingTop - 1 &&
		   y < GridHeight - gridPaddingTop)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.FenceRight;
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == gridPaddingTop - 1 &&
		   x > gridPaddingSides - 1 &&
		   x < GridWidth - gridPaddingSides)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.GetFenceBottom();
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == GridHeight - gridPaddingTop &&
		   x > gridPaddingSides - 1 &&
		   x < GridWidth - gridPaddingSides)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.GetFenceTop();
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == GridHeight - gridPaddingTop &&
		   x == GridWidth - gridPaddingSides)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.TopRight;
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == GridHeight - gridPaddingTop &&
		   x == gridPaddingSides - 1)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.TopLeft;
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == gridPaddingTop - 1 &&
		   x == GridWidth - gridPaddingSides)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.BottomRight;
			SetFenceCollider(render.gameObject);
			return;
		}

		if(y == gridPaddingTop - 1 &&
		   x == gridPaddingSides - 1)
		{
			render.transform.localScale = new Vector3(5,5);
			render.sprite = sprites.BottomLeft;
			SetFenceCollider(render.gameObject);
			return;
		}

		render.sprite = sprites.Grass;
	}
}
