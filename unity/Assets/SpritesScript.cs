﻿using UnityEngine;
using System.Collections;

public class SpritesScript : MonoBehaviour {

	public Sprite Grass;

	public Sprite FenceBottom;
	public Sprite FenceBottom2;
	public Sprite FenceBottom3;

	public Sprite FenceTop;
	public Sprite FenceTop2;
	public Sprite FenceTop3;

	public Sprite FenceLeft;
	public Sprite FenceRight;

	public Sprite TopLeft;
	public Sprite TopRight;
	public Sprite BottomRight;
	public Sprite BottomLeft;

	void Start () {
	
	}

	public Sprite GetFenceTop()
	{
		var value = Random.Range(0, 5);
		if(value == 3)
		{
			return FenceTop3;
		}
		if(value == 4)
		{
			return FenceTop2;
		}
		return FenceTop;
	}

	public Sprite GetFenceBottom()
	{
		var value = Random.Range(0, 5);
		if(value == 3)
		{
			return FenceBottom2;
		}
		if(value == 4)
		{
			return FenceBottom3;
		}
		return FenceBottom;
	}
}
