using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour
{
	[SerializeField]
	private int _x;

	[SerializeField]
	private int _y;

	public int X { get{return _x;} set{_x = value; }}
	public int Y { get{return _y;} set{_y = value;} }

	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

