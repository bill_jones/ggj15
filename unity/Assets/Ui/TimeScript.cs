﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TimeScript : MonoBehaviour {

	private Text _text;
	
	void Start () {
		_text = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		var minutes = (int)Math.Floor(Time.timeSinceLevelLoad / 60);
		var seconds = (int)Math.Floor(Time.timeSinceLevelLoad % 60);
		_text.text = string.Format("{0}:{1:00}",minutes, seconds);
	}
}
