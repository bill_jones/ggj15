﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	public Transform target;            // The position that that camera will be following.
	public float smoothing = 6.0f;        // The speed with which the camera will be following.


	Vector3 offset;                     // The initial offset from the target.

	public float? xMin;
	public float? xMax;
	public float? yMin;
	public float? yMax;

	private Camera _camera;

	private GridScript _grid;
	void Start ()
	{
		// Calculate the initial offset.
		offset = transform.position - target.position;
		_grid = FindObjectOfType<GridScript>();
		_camera = GetComponent<Camera>();
	}
	
	void FixedUpdate ()
	{
		if(!xMin.HasValue)
		{
			var tile = _grid.GetTile(0,0);
			xMin = tile.transform.position.x;
			yMin = tile.transform.position.y;

			Debug.Log("XMin:" + xMin);
			Debug.Log("YMin:" + yMin);
		}

		if(!yMax.HasValue)
		{
			var tile = _grid.GetTile(_grid.GridWidth -1, _grid.GridHeight - 1);
			xMax = tile.transform.position.x;
			yMax = tile.transform.position.y;

			Debug.Log("XMax:" + xMax);
			Debug.Log("YMax:" + yMax);
		}

		// Create a postion the camera is aiming for based on the offset from the target.
		Vector3 targetCamPos = target.position + offset;

		var initialPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

		// Smoothly interpolate between the camera's current position and it's target position.
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);

		var newPostion = transform.position;

		var bottomLeft = _camera.ScreenToWorldPoint(new Vector3(0,0,0));
		var topRight = _camera.ScreenToWorldPoint(new Vector3(_camera.pixelWidth, _camera.pixelHeight,0));


		if(bottomLeft.x < xMin)
		{
			newPostion.Set(initialPosition.x, newPostion.y, newPostion.z);
			transform.position = newPostion;
		}

		if(topRight.x > xMax)
		{
			newPostion.Set(initialPosition.x, newPostion.y, newPostion.z);
			transform.position = newPostion;
		}

		if(bottomLeft.y < yMin)
		{
			newPostion.Set(newPostion.x, initialPosition.y, newPostion.z);
			transform.position = newPostion;
		}

		if(topRight.y > yMax)
		{
			newPostion.Set(newPostion.x, initialPosition.y, newPostion.z);
			transform.position = newPostion;
		}

	}
}