﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	public ScoreScript ScoreScript;
	public SpawnManager SpawnManager;
	public GameScript GameScript;

	public float ChangeThreashold;
	public float Speed;
	public Vector3 Target;
	private int flowerLevelCounter;
	private int flowerLevelCounter2;


	private Transform _topCheck;
	private Transform _bottomCheck;
	private Transform _leftCheck;
	private Transform _rightCheck;

	private float _lastFixedUpdate;
	private PlayerInput _playerInput;

	public bool Moving {get; private set;}
	public bool Collecting;
	public bool SteppingOnShit;
	public bool Turning;

	public AudioSource CollectingSound;
	public AudioSource ShitSound;
	public AudioSource TurningSound;

	void Start()
	{
		_lastFixedUpdate = Time.fixedTime;
		Moving = false;
		SpawnManager = FindObjectOfType <SpawnManager> ();
		GameScript = FindObjectOfType <GameScript> ();
		Collecting = false;
		SteppingOnShit = false;
		flowerLevelCounter = 0;
		flowerLevelCounter2 = 0;
		Target = transform.position;
		_playerInput = GetComponent<PlayerInput>();

		_topCheck = transform.Find("TopCheck");
		_bottomCheck = transform.Find("BottomCheck");
		_leftCheck = transform.Find("LeftCheck");
		_rightCheck = transform.Find("RightCheck");
	}

	void Update()
	{
		float step = Speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, Target, step);
		if(transform.position != Target)
		{
			Moving = true;
		}
		else 
		{
			Moving = false;
		}
		if (Turning)
		{
			TurningSound.Play ();
			Turning = false;
		}
		if (Collecting)
		{
			CollectingSound.Play ();
			Collecting = false;
		}
		else if (SteppingOnShit)
		{
			ShitSound.Play ();
			SteppingOnShit = false;
		}
		if (Time.fixedTime == 20f)
		{
			SpawnManager.spawnTime /= 2f;
		}
		if (Time.fixedTime == 40f)
		{
			Speed *= 1.5f;
		}
		if (ScoreScript.Score < 0)
		{
			GameScript.GameOver();
		}
	}

	void FixedUpdate()
	{
		var sinceLastMove = Time.fixedTime - _lastFixedUpdate;
		if(sinceLastMove > ChangeThreashold)
		{
			_lastFixedUpdate = Time.fixedTime;

			var newPostion = transform.position + _playerInput.Adjustment;
			if(CheckCollision(newPostion, _bottomCheck.position + _playerInput.Adjustment)) return;
			if(CheckCollision(newPostion, _topCheck.position + _playerInput.Adjustment)) return;
			if(CheckCollision(newPostion, _rightCheck.position + _playerInput.Adjustment)) return;
			if(CheckCollision(newPostion, _leftCheck.position + _playerInput.Adjustment)) return;

			Target = newPostion;
		}

	}

	private bool CheckCollision(Vector3 newPosition, Vector3 check)
	{
		return Physics2D.Linecast(newPosition, check, 1 << LayerMask.NameToLayer("Fence")); 
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Flower")
		{
			Collecting = true;
			Debug.Log ("flower!");
			ScoreScript.Score += 1;
			if (ScoreScript.Score == 4)
			{
				flowerLevelCounter += 1;
				if (flowerLevelCounter == 1)
				{
					Speed *= 1.5f;
					ChangeThreashold /= 2f;
				}
			}
			else if (ScoreScript.Score == 10)
			{
				flowerLevelCounter2 += 1;
				if (flowerLevelCounter2 == 1)
				{
					ChangeThreashold /= 2f;
				}
			}
			Destroy (other.gameObject);
		}

		else if (other.gameObject.tag == "Poop")
		{
			SteppingOnShit = true;
			Debug.Log ("Poop!");
			ScoreScript.Score -= 3;
			Destroy (other.gameObject);
		}
		
	}
}
