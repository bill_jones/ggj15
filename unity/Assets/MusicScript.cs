﻿using UnityEngine;
using System.Collections;

public class MusicScript : MonoBehaviour {

	public AudioSource MarchStart;
	public AudioSource MarchLoop;
	public AudioSource MarchEnd;

	public AudioSource Menu;

	private bool _startWasPlaying;
	private bool _paused;
	private AudioSource _musicPlaying;
	void Start()
	{
	}

	void Update()
	{
		if(MarchStart.audio.isPlaying)
		{
			_startWasPlaying = true;
		}
		else if (_startWasPlaying && !_paused)
		{
			MarchLoop.Play();
			_startWasPlaying = false;
		}
	}

	public void ResumeMusic()
	{
		_paused = false;
		_musicPlaying.Play();
	}

	public void PauseMusic()
	{
		_paused = true;
		if(MarchStart.isPlaying)
		{
			MarchStart.audio.Pause();
			_musicPlaying = MarchStart;
		}

		if(MarchLoop.isPlaying)
		{
			MarchLoop.audio.Pause();
			_musicPlaying = MarchLoop;
		}

		if(MarchEnd.isPlaying)
		{
			MarchEnd.audio.Pause();
			_musicPlaying = MarchEnd;
		}
	}
}
