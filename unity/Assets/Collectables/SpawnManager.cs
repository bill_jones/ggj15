﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

	public GameObject collectable;
	public int spawnObjectIdx;
	public Transform[] spawnPoints;
	public float spawnTime;
	public GridScript GridScript;
	private int row;
	private int col;

	public GameObject FlowerPrefab;
	public GameObject PoopPrefab;
	
	// Use this for initialization
	void Start () {
     	GridScript = FindObjectOfType <GridScript> ();
		if (GridScript != null) {
			row = GridScript.GridHeight;
			col = GridScript.GridWidth;
	
			InvokeRepeating ("Spawn", spawnTime*3, spawnTime);
		}
		else
		{
			Debug.Log("no such component"); 
		}
	}
	
	// Update is called once per frame
	void Spawn () {
		//if the game ends, stop spawning
		GameObject[,] _tiles = GridScript._tiles;
		if (_tiles == null) return;

		spawnObjectIdx = Random.Range (0, 2);
		//int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		//0 stands for flower
		if (spawnObjectIdx == 0) 
		{
			collectable = FlowerPrefab;
		}
		//1 stands for poop
		else if (spawnObjectIdx == 1)
		{
			collectable = PoopPrefab;
		}

		if (collectable == null) return;
		var i = Random.Range (0, col);
		var j = Random.Range (0, row);


		Instantiate (collectable, _tiles [i, j].transform.position, _tiles [i, j].transform.rotation);
		//Instantiate (collectable, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
	}
}
