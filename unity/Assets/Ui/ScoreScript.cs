﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour {

	public int Score {get; set;}

	private Text _text;

	void Start() {
		Score = 0;
		_text = GetComponentInChildren<Text>();
	}

	void Update() {
		_text.text = Score.ToString();
	}
}
