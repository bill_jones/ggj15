﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameScript : MonoBehaviour {
	
	public GameObject IntroPanel;
	public GameObject PausePanel;
	public GameObject InGamePanel;
	public ScoreScript _score;
	public Text _gameoverText;
	
	private bool _gameStarted;
	private bool _gameOver;
	private MusicScript _music;

	// Use this for initialization
	void Start () {
		_gameStarted = false;
		_gameOver = false;
		_gameoverText.text = "";
		_score = FindObjectOfType <ScoreScript> ();
		if (_score == null)
		{
			Debug.Log ("no score");
		}
		IntroPanel.SetActive(true);
		PausePanel.SetActive(false);
		InGamePanel.SetActive(false);
		_music = FindObjectOfType<MusicScript>();
		Time.timeScale = 0f;
	}

	public void StartGame() {
		_gameStarted = true;
		IntroPanel.SetActive(false);
		InGamePanel.SetActive(true);
		Time.timeScale = 1.0f;
		_music.Menu.Stop();
		_music.MarchStart.audio.Play();
	}

	public void ResumeGame() {
		PausePanel.SetActive(false);
		Time.timeScale = 1.0f;
		_music.ResumeMusic();
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void GameOver ()
	{
		_gameoverText.text = "Game Over!";
		_gameOver = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) && _gameStarted) {
			Debug.Log("Escape Pressed");
			_music.PauseMusic();
			Time.timeScale = 0f;
			PausePanel.SetActive(true);
		}
	}
}
