﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GridScript))]
public class GridHelper : Editor {

	public override void OnInspectorGUI ()
	{
		var gridScript = (GridScript)this.target;

		gridScript.GridWidth = EditorGUILayout.IntField("Grid Width", gridScript.GridWidth);
		gridScript.GridHeight = EditorGUILayout.IntField("Grid Height", gridScript.GridHeight);
		gridScript.TileSpacing = EditorGUILayout.FloatField("Tile Spacing", gridScript.TileSpacing);
		gridScript.TilePrefab = (GameObject)EditorGUILayout.ObjectField("Title Prefab", gridScript.TilePrefab, typeof(GameObject), false);

		gridScript.SpriteManager = (GameObject)EditorGUILayout.ObjectField("Sprite Manager", gridScript.SpriteManager, typeof(GameObject), false);


		if (GUI.Button(new Rect(20, 500, 100, 30), new GUIContent("Create Grid")))
		{
			gridScript.CreateGrid();
		}
	}
}
