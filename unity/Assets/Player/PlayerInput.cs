using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
	public Sprite Forward;
	public Sprite Left;
	public Sprite Right;
	public Sprite Back;
	public int Direction;
	private int LastDirection;

	private SpriteRenderer _render;
	private PlayerController _controller;
	private Animator _animator;
	
	public Vector3 Adjustment {get; private set;}

	void Start()
	{
		_render = GetComponent<SpriteRenderer>();
		_controller = GetComponent<PlayerController>();
		_animator = GetComponent<Animator>();
		FaceSouth();
		LastDirection = Direction;
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			HandleLeft();
		}

		else if (Input.GetKeyDown (KeyCode.R))
		{
			Application.LoadLevel (Application.loadedLevel);
		}
		/*
		else if(Input.GetKeyDown(KeyCode.RightArrow) && !_controller.Moving)
		{
			HandleRight();
		}
		*/
	}

	void HandleLeft()
	{
		if(Direction == _South)
		{
			FaceLeft();
		}
		else if(Direction == _Left)
		{
			FaceNorth();
		}
		else if(Direction == _North)
		{
			FaceRight();
		}
		else if(Direction == _Right)
		{
			FaceSouth();
		}
	}

	void HandleRight()
	{
		if(Direction == _South)
		{
			FaceRight();
		}
		else if(Direction == _Left)
		{
			FaceSouth();
		}
		else if(Direction == _North)
		{
			FaceLeft();
		}
		else if(Direction == _Right)
		{
			FaceNorth();
		}
	}

	private static string Dir = "Direction";
	private static int _Left = 1;
	private static int _South = 0;
	private static int _North = 2;
	private static int _Right = 3;

	void FaceLeft()
	{
		_render.sprite = Left;
		_animator.SetInteger(Dir, _Left); 
		Direction = _Left;
		Adjustment = new Vector3(-5, 0);
	}

	void FaceRight()
	{
		_render.sprite = Right;
		_animator.SetInteger(Dir, _Right);
		Direction = _Right;
		Adjustment = new Vector3(5, 0);
	}

	void FaceNorth()
	{
		_render.sprite = Back;
		_animator.SetInteger(Dir, _North);
		Direction = _North;
		Adjustment = new Vector3(0, 5);
	}

	void FaceSouth()
	{
		_render.sprite = Forward;
		_animator.SetInteger(Dir, _South);
		Direction = _South;
		Adjustment = new Vector3(0, -5);

	}
}

